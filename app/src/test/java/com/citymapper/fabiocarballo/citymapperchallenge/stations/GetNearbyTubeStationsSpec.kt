package com.citymapper.fabiocarballo.citymapperchallenge.stations

import com.citymapper.fabiocarballo.citymapperchallenge.api.ApiTubeStationBundleFormat
import com.citymapper.fabiocarballo.citymapperchallenge.api.ApiTubeStationFormat
import com.citymapper.fabiocarballo.citymapperchallenge.api.TransportsForLondonUnifiedApi
import com.citymapper.fabiocarballo.citymapperchallenge.internals.exceptions.NoConnectionException
import com.citymapper.fabiocarballo.citymapperchallenge.location.CoordinatePoint
import com.citymapper.fabiocarballo.citymapperchallenge.location.DistancesCalculator
import com.citymapper.fabiocarballo.citymapperchallenge.location.GetLocationUseCase
import com.nhaarman.mockito_kotlin.*
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.*
import rx.observers.TestSubscriber
import rx.subjects.PublishSubject

class GetNearbyTubeStationsSpec : Spek({

    val getLocationUseCase: GetLocationUseCase = mock()
    val api: TransportsForLondonUnifiedApi = mock()
    val distancesCalculator: DistancesCalculator = mock()

    given("Get Nearby Tube Stations") {

        val tested = GetNearbyTubeStations(getLocationUseCase, api, distancesCalculator)

        describe("#build") {
            var testSubscriber: TestSubscriber<List<NearbyTubeStation>> = TestSubscriber()
            var locationPubSub: PublishSubject<CoordinatePoint> = PublishSubject.create()
            var nearbyRequestPubSub: PublishSubject<ApiTubeStationBundleFormat> = PublishSubject.create()

            beforeEachTest {
                reset(getLocationUseCase)
                reset(api)

                testSubscriber = TestSubscriber()
                locationPubSub = PublishSubject.create()
                nearbyRequestPubSub = PublishSubject.create()

                whenever(getLocationUseCase.build()).thenReturn(locationPubSub)
                whenever(api.getTubeStations(any(), any(), any(), any(), any(), any(), any()))
                        .thenReturn(nearbyRequestPubSub.toSingle())

                tested.build().subscribe(testSubscriber)
            }

            context("location is returned") {
                val coordinatePoint = CoordinatePoint(10.343454, -0.43445)

                beforeEachTest {
                    locationPubSub.onNext(coordinatePoint)
                }

                it("should request the stations near that location") {
                    verify(api).getTubeStations(
                            eq(coordinatePoint.latitude),
                            eq(coordinatePoint.longitude),
                            any(), any(), any(), any(), any())
                }

                context("api replies with stations") {
                    val tubeStations = listOf(
                            ApiTubeStationFormat("id1", "station-1", 10.343454, -0.43445),
                            ApiTubeStationFormat("id2", "station-2", 10.343456, -0.43445),
                            ApiTubeStationFormat("id3", "station-3", 10.343454, -0.43445))

                    beforeEachTest {
                        whenever(distancesCalculator.distanceBetween(any(), any()))
                                .thenReturn(200f, 300f, 100f)

                        nearbyRequestPubSub.onNext(ApiTubeStationBundleFormat(tubeStations))
                        nearbyRequestPubSub.onCompleted()
                    }

                    it("should return the list of nearby stations sorted by distance") {
                        val expected = listOf(
                                NearbyTubeStation("id3", "station-3", 100f, CoordinatePoint(10.343454, -0.43445)),
                                NearbyTubeStation("id1", "station-1", 200f, CoordinatePoint(10.343454, -0.43445)),
                                NearbyTubeStation("id2", "station-2", 300f, CoordinatePoint(10.343456, -0.43445)))

                        testSubscriber.assertValue(expected)
                        testSubscriber.assertCompleted()
                    }
                }

                context("request fails") {
                    val exception = NoConnectionException()

                    beforeEachTest {
                        nearbyRequestPubSub.onError(exception)
                    }

                    it("should return the exception") {
                        testSubscriber.assertNoValues()
                        testSubscriber.assertError(exception)
                    }
                }
            }

            context("exception happens fetching location") {
                val exception = Exception("Something bad happened!")

                beforeEachTest {
                    locationPubSub.onError(exception)
                }

                it("should return the exception") {
                    testSubscriber.assertNoValues()
                    testSubscriber.assertError(exception)
                }
            }
        }
    }
})