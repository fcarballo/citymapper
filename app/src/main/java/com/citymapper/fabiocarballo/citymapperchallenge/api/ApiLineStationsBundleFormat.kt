package com.citymapper.fabiocarballo.citymapperchallenge.api

import com.google.gson.annotations.SerializedName

data class ApiLineStationsBundleFormat(
        val stations: List<ApiLineStopPointFormat>,
        val orderedLineRoutes: List<ApiLineRouteFormat>,
        val stopPointSequences: List<ApiLineStopPointSequence>)

data class ApiLineStopPointSequence(
        @SerializedName("stopPoint")
        val stopPoints: List<ApiLineStopPointFormat>)

data class ApiLineRouteFormat(
        @SerializedName("naptanIds")
        val ids: List<String>)