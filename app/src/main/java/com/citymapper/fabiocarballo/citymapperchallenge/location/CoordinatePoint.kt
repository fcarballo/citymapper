package com.citymapper.fabiocarballo.citymapperchallenge.location

import android.location.Location

data class CoordinatePoint(
        val latitude: Double,
        val longitude: Double) {

    fun distanceTo(coordinatePoint: CoordinatePoint): Float {
        val results = FloatArray(2)
        Location.distanceBetween(latitude, longitude, coordinatePoint.latitude, coordinatePoint.longitude, results)

        return results.first()
    }
}