package com.citymapper.fabiocarballo.citymapperchallenge.internals.di

import android.app.Activity
import com.citymapper.fabiocarballo.citymapperchallenge.internals.Navigator
import com.citymapper.fabiocarballo.citymapperchallenge.location.LocationSettingsSolver
import com.citymapper.fabiocarballo.citymapperchallenge.location.LocationStatusManager
import dagger.Module
import dagger.Provides
import pl.charmas.android.reactivelocation.ReactiveLocationProvider

@Module
class ActivityModule(private val activity: Activity) {

    @Provides
    @ActivityScope
    fun provideActivity(): Activity {
        return activity
    }

    @Provides
    @ActivityScope
    fun provideLocationStatusManager(reactiveLocationProvider: ReactiveLocationProvider): LocationStatusManager {
        return LocationStatusManager(reactiveLocationProvider, activity)
    }

    @Provides
    fun provideLocationSettingsSolver(reactiveLocationProvider: ReactiveLocationProvider): LocationSettingsSolver {
        return LocationSettingsSolver(reactiveLocationProvider, activity)
    }

    @Provides
    fun provideNavigator(): Navigator {
        return Navigator(activity)
    }
}