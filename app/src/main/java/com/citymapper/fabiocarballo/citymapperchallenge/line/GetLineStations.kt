package com.citymapper.fabiocarballo.citymapperchallenge.line

import com.citymapper.fabiocarballo.citymapperchallenge.api.TransportsForLondonUnifiedApi
import com.citymapper.fabiocarballo.citymapperchallenge.location.CoordinatePoint
import com.citymapper.fabiocarballo.citymapperchallenge.stations.TubeStation
import rx.Single

class GetLineStations(
        private val api: TransportsForLondonUnifiedApi) {

    fun build(lineId: String, inbound: Boolean): Single<List<List<TubeStation>>> {
        val lineStationsOservable = if (inbound) {
            api.getInboundLineStations(lineId)
        } else {
            api.getOutboundLineStations(lineId)
        }

        return lineStationsOservable
                .map {
                    it.stopPointSequences
                            .map { stopPointSequence ->
                                stopPointSequence.stopPoints
                                        .map { TubeStation(it.id, it.name, CoordinatePoint(it.lat, it.lon)) }
                            }
                }
    }
}
