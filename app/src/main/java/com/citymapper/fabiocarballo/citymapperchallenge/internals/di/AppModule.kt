package com.citymapper.fabiocarballo.citymapperchallenge.internals.di

import android.content.Context
import com.citymapper.fabiocarballo.citymapperchallenge.api.TransportsForLondonUnifiedApi
import com.citymapper.fabiocarballo.citymapperchallenge.internals.Logger
import com.citymapper.fabiocarballo.citymapperchallenge.internals.SystemTimeProvider
import com.citymapper.fabiocarballo.citymapperchallenge.internals.networking.RetrofitFactory
import com.citymapper.fabiocarballo.citymapperchallenge.location.GetLocationUseCase
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import pl.charmas.android.reactivelocation.ReactiveLocationProvider
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideReactiveLocationProvider(): ReactiveLocationProvider {
        return ReactiveLocationProvider(context)
    }

    @Provides
    @Singleton
    fun provideGetLocationUseCase(reactiveLocationProvider: ReactiveLocationProvider): GetLocationUseCase {
        return GetLocationUseCase(reactiveLocationProvider)
    }

    @Provides
    @Singleton
    fun provideLogger(): Logger {
        return Logger()
    }

    @Provides
    @Singleton
    fun provideSystemTimeProvider(): SystemTimeProvider {
        return object : SystemTimeProvider {
            override fun timeInMillis(): Long {
                return System.currentTimeMillis()
            }
        }
    }
}