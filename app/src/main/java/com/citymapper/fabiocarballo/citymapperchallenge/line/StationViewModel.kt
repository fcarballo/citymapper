package com.citymapper.fabiocarballo.citymapperchallenge.line

data class StationViewModel(
        val id: String,
        val name: String)