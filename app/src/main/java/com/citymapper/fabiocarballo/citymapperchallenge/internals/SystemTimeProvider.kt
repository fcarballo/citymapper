package com.citymapper.fabiocarballo.citymapperchallenge.internals

interface SystemTimeProvider {

    fun timeInMillis(): Long
}