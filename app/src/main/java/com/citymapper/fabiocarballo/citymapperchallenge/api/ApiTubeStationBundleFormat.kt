package com.citymapper.fabiocarballo.citymapperchallenge.api

data class ApiTubeStationBundleFormat(
        val stopPoints: List<ApiTubeStationFormat>)