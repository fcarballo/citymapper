package com.citymapper.fabiocarballo.citymapperchallenge.location


import android.app.Activity
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationSettingsRequest
import pl.charmas.android.reactivelocation.ReactiveLocationProvider
import rx.Completable

/**
 * The goal of this class is to wrap the GoogleApi flow to present the location dialog to the user.
 */
class LocationSettingsSolver(
        private val reactiveLocationProvider: ReactiveLocationProvider,
        private val activity: Activity) {

    private var locationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

    private val REQUEST_CHECK_SETTINGS = 1025

    private fun buildLocationSettingsRequest(locationRequest: LocationRequest): LocationSettingsRequest {
        return LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
                .setAlwaysShow(true)
                .build()
    }

    /**
     * This method is responsible for calling the GoogleApi methods so that the location dialog
     * is shown. With that dialog the user may turn on his.
     *
     * It returns the "hasResolution" because the Status code returned by the GoogleApi may not
     * be "solvable".
     */
    fun solveLocation(): Completable {
        return reactiveLocationProvider.checkLocationSettings(buildLocationSettingsRequest(locationRequest))
                .map { it.status }
                .doOnNext { status ->
                    status.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS)
                }
                .first()
                .toCompletable()
    }

    fun requestIsSolved(requestCode: Int, resultCode: Int): Boolean {
        return requestCode == REQUEST_CHECK_SETTINGS && resultCode == Activity.RESULT_OK
    }
}