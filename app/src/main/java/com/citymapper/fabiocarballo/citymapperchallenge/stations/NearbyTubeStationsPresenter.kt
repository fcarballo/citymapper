package com.citymapper.fabiocarballo.citymapperchallenge.stations

import com.citymapper.fabiocarballo.citymapperchallenge.internals.Logger
import com.citymapper.fabiocarballo.citymapperchallenge.internals.Navigator
import com.citymapper.fabiocarballo.citymapperchallenge.internals.SystemTimeProvider
import com.citymapper.fabiocarballo.citymapperchallenge.internals.exceptions.NoConnectionException
import com.citymapper.fabiocarballo.citymapperchallenge.location.LocationSettingsStatus
import com.citymapper.fabiocarballo.citymapperchallenge.location.LocationStatusManager
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

class NearbyTubeStationsPresenter(
        private val view: View,
        private val locationManager: LocationStatusManager,
        private val getNearbyTubeStations: GetNearbyTubeStations,
        private val getArrivalsForTubeStation: GetArrivalsForTubeStation,
        private val systemTimeProvider: SystemTimeProvider,
        private val navigator: Navigator,
        private val logger: Logger) {

    private val subscriptions = CompositeSubscription()

    fun start() {
        subscriptions.add(
                locationManager.locationSettingsStatus()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { locationSettingsStatus: LocationSettingsStatus ->
                                    when (locationSettingsStatus) {
                                        LocationSettingsStatus.OK -> {
                                            getNearbyTubeStations()
                                            view.hideDisabledLocationAlert()
                                        }
                                        LocationSettingsStatus.LOCATION_DISABLED -> {
                                            view.showLocationDialog()
                                            view.showDisabledLocationAlert()
                                        }
                                        LocationSettingsStatus.REQUIRES_PERMISSION -> {
                                            locationManager.requestLocationPermission()
                                            view.showDisabledLocationAlert()
                                        }
                                    }
                                },
                                {
                                    logger.error(it)

                                    view.showUnknownErrorPlaceholder()
                                }
                        )
        )
    }

    fun stop() {
        subscriptions.clear()
    }

    fun onArrivalClick(arrival: TubeArrival) {
        navigator.openLineScreen(arrival)
    }

    private fun getNearbyTubeStations() {
        logger.debug("requesting nearby tube stations next arrivals ...")
        subscriptions.add(
                getNearbyTubeStations.build()
                        .doOnSuccess { logger.debug("got ${it.size} nearby stations") }
                        .flatMapObservable { arrivals(it) }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { stationsArrivalsList ->
                                    logger.debug("got arrivals for ${stationsArrivalsList.size} stations.")

                                    view.setStationArrivals(stationsArrivalsList, systemTimeProvider.timeInMillis())
                                },
                                handleError(),
                                { logger.debug("finished fetching arrivals.") }))
    }

    private fun arrivals(stations: List<NearbyTubeStation>): Observable<List<StationArrivalsViewModel>> {
        val timerObservable = Observable.interval(TIME_INTERVAL_SECONDS, TimeUnit.SECONDS)
                .doOnNext { logger.debug("$TIME_INTERVAL_SECONDS seconds were reached. will fetch arrivals times!") }

        return Observable.just(0L) // just to start the first trigger without the delay
                .concatWith(timerObservable)
                .flatMap {
                    val stationsArrivalsObservables: Array<out Observable<StationArrivalsViewModel>> =
                            stations.map { station ->
                                getArrivalsForTubeStation.build(station.id)
                                        .toObservable()
                                        .map { StationArrivalsViewModel(station.name, station.distanceInMeters.toInt(), it) }
                            }.toTypedArray()

                    Observable.zip(stationsArrivalsObservables, { it.toList() as List<StationArrivalsViewModel> })
                }
    }

    private fun handleError(): ((Throwable) -> Unit) = {
        logger.error(it)

        when (it) {
            is NoConnectionException -> view.showConnectionIssuesPlaceholder()
            is TimeoutException -> view.showConnectionIssuesPlaceholder()
            else -> view.showUnknownErrorPlaceholder()
        }
    }

    companion object {
        private const val TIME_INTERVAL_SECONDS = 30L
    }

    interface View {
        fun showLocationDialog()
        fun showDisabledLocationAlert()
        fun hideDisabledLocationAlert()
        fun setStationArrivals(stationsArrivalsList: List<StationArrivalsViewModel>, timestampInMillis: Long)
        fun showConnectionIssuesPlaceholder()
        fun showUnknownErrorPlaceholder()
    }
}