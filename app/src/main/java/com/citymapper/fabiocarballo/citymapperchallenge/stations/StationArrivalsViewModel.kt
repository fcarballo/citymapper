package com.citymapper.fabiocarballo.citymapperchallenge.stations

import org.parceler.Parcel

@Parcel
data class StationArrivalsViewModel(
        val name: String = "",
        val distanceInMeters: Int = 0,
        val arrivals: List<TubeArrival> = listOf())