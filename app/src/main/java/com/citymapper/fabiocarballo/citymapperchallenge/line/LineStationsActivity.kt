package com.citymapper.fabiocarballo.citymapperchallenge.line

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import com.citymapper.fabiocarballo.citymapperchallenge.R
import com.citymapper.fabiocarballo.citymapperchallenge.internals.BaseActivity
import com.citymapper.fabiocarballo.citymapperchallenge.internals.Logger
import com.citymapper.fabiocarballo.citymapperchallenge.internals.toGone
import com.citymapper.fabiocarballo.citymapperchallenge.internals.toVisible
import com.citymapper.fabiocarballo.citymapperchallenge.location.LocationSettingsSolver
import com.citymapper.fabiocarballo.citymapperchallenge.location.LocationStatusManager
import com.citymapper.fabiocarballo.citymapperchallenge.stations.TubeArrival
import com.citymapper.fabiocarballo.citymapperchallenge.ui.SnackbarDisplayer
import kotlinx.android.synthetic.main.activity_line_stations.*
import kotlinx.android.synthetic.main.view_error.*
import kotlinx.android.synthetic.main.view_location_simulator.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.onCheckedChange
import org.parceler.Parcels
import rx.subscriptions.Subscriptions
import javax.inject.Inject

class LineStationsActivity : BaseActivity(), LineStationsPresenter.View {

    @Inject
    lateinit var presenter: LineStationsPresenter

    @Inject
    lateinit var locationSettingsSolver: LocationSettingsSolver

    @Inject
    lateinit var logger: Logger

    private val snackbarDisplayer by lazy { SnackbarDisplayer(refreshLayout) }

    private var locationSolverSubscription = Subscriptions.empty()

    private val stationsAdapter by lazy { LineStationAdapter(this, tubeArrival.arrivalStationId) }

    private val tubeArrival: TubeArrival by lazy { Parcels.unwrap<TubeArrival>(intent?.getParcelableExtra(TUBE_ARRIVAL)) }

    private var lastUserJourneyPoint: UserJourney? = null

    private val isSimulation: Boolean
        get() = locationSimulatorSwitch.isChecked

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_line_stations)

        activityComponent
                .lineStationsScreenComponentBuilder()
                .plus(LineStationsScreenModule(tubeArrival, this))
                .build()
                .inject(this)

        supportActionBar?.let {
            it.title = tubeArrival.lineName
            it.setDisplayHomeAsUpEnabled(true)
        }

        with(recyclerView) {
            layoutManager = android.support.v7.widget.LinearLayoutManager(this@LineStationsActivity)
            adapter = stationsAdapter
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    lastUserJourneyPoint?.let { updateUserJourney(it) }
                }
            })
        }

        refreshLayout.setColorSchemeColors(
                ContextCompat.getColor(this, R.color.yellow),
                ContextCompat.getColor(this, R.color.green))

        refreshLayout.setOnRefreshListener {
            startLineFetching(true)
        }

        locationSimulatorSwitch.onCheckedChange { _, _ -> startLineFetching() }

        startLineFetching()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.stop()

        locationSolverSubscription.unsubscribe()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // this was triggered by the permissions flow, let's start our location settings verification flow again
        if (locationSettingsSolver.requestIsSolved(requestCode, resultCode)) {
            startLineFetching()
        } else {
            displayError(R.string.error_location)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // this was triggered by the permissions flow, let's start our location settings verification flow again
        if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
            startLineFetching()
        } else {
            displayError(R.string.error_location_permissions)
        }
    }

    private fun startLineFetching(isRefresh: Boolean = false) {
        if (!isRefresh) {
            loadingIcon.toVisible()
        }

        userPosition.toGone()
        lineView.toGone()

        presenter.start(isSimulation)
    }

    override fun showLocationDialog() {
        locationSolverSubscription = locationSettingsSolver.solveLocation()
                .subscribe(
                        { logger.debug("Executed location solving flow.") },
                        { logger.error(it) })
    }

    override fun showDisabledLocationAlert() {
        snackbarDisplayer.show(R.string.location_prompt_request, R.string.location_prompt_action) {
            startLineFetching()
        }
    }

    override fun hideDisabledLocationAlert() {
        snackbarDisplayer.hide()
    }

    fun setLoadingIconVisibility(visible: Boolean) {
        if (visible) {
            loadingIcon.toVisible()
        } else {
            refreshLayout.isRefreshing = false
            loadingIcon.toGone()
        }
    }

    override fun setStations(stations: List<StationViewModel>) {
        setLoadingIconVisibility(false)

        errorView.toGone()
        lineView.toVisible()

        stationsAdapter.stationsList = stations
        stationsAdapter.notifyDataSetChanged()
    }

    override fun updateUserJourney(userJourney: UserJourney) {
        val leavingStationIndex = stationsAdapter.indexOf(userJourney.startingStationId)
        val approachingStationIndex = stationsAdapter.indexOf(userJourney.finalStationId)

        val leavingStationView = recyclerView.layoutManager.findViewByPosition(leavingStationIndex)
        val approachingStationView = recyclerView.layoutManager.findViewByPosition(approachingStationIndex)

        lastUserJourneyPoint = userJourney

        if (leavingStationView != null && approachingStationView != null) {
            userPosition.toVisible()

            val journeyStartY = leavingStationView.top + leavingStationView.height / 2
            val journeyEndY = approachingStationView.top + approachingStationView.height / 2

            val offset = (journeyEndY - journeyStartY) * userJourney.journeyPercentage

            userPosition.y = journeyStartY + offset
        } else {
            userPosition.toGone()
        }
    }

    override fun showConnectionIssuesPlaceholder() {
        displayError(R.string.error_network)
    }

    override fun showUnknownErrorPlaceholder() {
        displayError(R.string.error_unknown)
    }

    private fun displayError(@StringRes msgResId: Int) {
        setLoadingIconVisibility(false)

        lineView.toGone()
        errorView.toVisible()

        errorMessage.text = getString(msgResId)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }

        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val TUBE_ARRIVAL = "tubeArrival"

        fun buildIntent(context: Context,
                        arrival: TubeArrival): Intent {
            return context.intentFor<LineStationsActivity>(TUBE_ARRIVAL to Parcels.wrap(arrival))
        }
    }
}