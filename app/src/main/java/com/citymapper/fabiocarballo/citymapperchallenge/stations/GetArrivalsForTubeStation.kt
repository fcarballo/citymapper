package com.citymapper.fabiocarballo.citymapperchallenge.stations

import com.citymapper.fabiocarballo.citymapperchallenge.api.TransportsForLondonUnifiedApi
import com.citymapper.fabiocarballo.citymapperchallenge.internals.Logger
import rx.Single

class GetArrivalsForTubeStation(
        private val api: TransportsForLondonUnifiedApi,
        private val logger: Logger) {

    fun build(tubeStationId: String, numArrivals: Int = 3): Single<List<TubeArrival>> {
        return api.getNextArrivals(tubeStationId)
                .doOnSuccess { logger.debug("Got arrivals from api: $it") }
                .map { arrivals ->
                    arrivals
                            .filter { it.isValid() }
                            .map {
                                TubeArrival(
                                        tubeStationId,
                                        it.lineId,
                                        it.lineName,
                                        it.expectedArrival,
                                        it.destinationId,
                                        it.destinationName!!,
                                        it.direction == KEYWORD_INBOUND)
                            }
                            .sortedBy { it.expectedTime }
                            .take(numArrivals)
                }
    }

    companion object {
        private const val KEYWORD_INBOUND = "inbound"
    }

}