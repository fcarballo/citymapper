package com.citymapper.fabiocarballo.citymapperchallenge.line

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import com.citymapper.fabiocarballo.citymapperchallenge.R

class CircleView : View {

    private val circlePaint by lazy {
        Paint().apply {
            color = ContextCompat.getColor(context, R.color.blue)
            isAntiAlias = true
            style = Paint.Style.FILL
        }
    }

    constructor(context: Context?) : super(context)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val radius = Math.min(width, height) / 2f

        canvas.drawCircle((width / 2f), height / 2f, radius, circlePaint)
    }


}