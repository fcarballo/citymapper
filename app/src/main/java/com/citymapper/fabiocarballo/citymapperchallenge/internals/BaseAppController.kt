package com.citymapper.fabiocarballo.citymapperchallenge.internals

import android.app.Application
import com.citymapper.fabiocarballo.citymapperchallenge.internals.di.AppModule
import com.citymapper.fabiocarballo.citymapperchallenge.internals.di.BaseAppComponent
import com.citymapper.fabiocarballo.citymapperchallenge.internals.di.DaggerAppComponent

abstract class BaseAppController : Application() {

    val appComponent: BaseAppComponent by lazy {
        DaggerAppComponent.builder()
                .appModule(AppModule(this@BaseAppController))
                .build()
    }
}