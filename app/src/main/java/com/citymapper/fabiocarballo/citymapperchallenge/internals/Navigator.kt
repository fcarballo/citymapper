package com.citymapper.fabiocarballo.citymapperchallenge.internals

import android.content.Context
import com.citymapper.fabiocarballo.citymapperchallenge.line.LineStationsActivity
import com.citymapper.fabiocarballo.citymapperchallenge.stations.TubeArrival

class Navigator(private val context: Context) {

    fun openLineScreen(arrival: TubeArrival) {
        context.startActivity(LineStationsActivity.buildIntent(context, arrival))
    }
}