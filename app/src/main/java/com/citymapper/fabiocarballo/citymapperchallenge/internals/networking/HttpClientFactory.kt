package com.citymapper.fabiocarballo.citymapperchallenge.internals.networking

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

class HttpClientFactory {

    fun build(interceptor: Interceptor? = null): OkHttpClient {
        val builder = OkHttpClient.Builder().apply {
            addInterceptor { chain ->
                val original = chain.request()
                val originalHttpUrl = original.url()

                val url = originalHttpUrl.newBuilder()
                        .addQueryParameter(PARAM_APP_ID, APP_ID)
                        .addQueryParameter(PARAM_APP_KEY, APP_KEY)
                        .build()

                // Request customization: add request headers
                val requestBuilder = original.newBuilder()
                        .url(url)

                val request = requestBuilder.build()

                chain.proceed(request)
            }
        }

        interceptor?.let {
            builder.addNetworkInterceptor(it)
        }

        builder.addInterceptor(HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BASIC))

        return builder.build()
    }


    companion object {
        private const val PARAM_APP_ID = "app_id"
        private const val PARAM_APP_KEY = "app_key"

        private const val APP_ID = "295d6f34"
        private const val APP_KEY = "54cfe072beb81144420ea8fe29c28a0c"
    }
}