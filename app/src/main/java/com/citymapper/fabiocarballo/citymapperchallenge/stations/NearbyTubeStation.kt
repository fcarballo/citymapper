package com.citymapper.fabiocarballo.citymapperchallenge.stations

import com.citymapper.fabiocarballo.citymapperchallenge.location.CoordinatePoint

data class NearbyTubeStation(
        val id: String,
        val name: String,
        val distanceInMeters: Float,
        val location: CoordinatePoint)