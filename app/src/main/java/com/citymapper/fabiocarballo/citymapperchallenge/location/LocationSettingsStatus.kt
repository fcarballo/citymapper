package com.citymapper.fabiocarballo.citymapperchallenge.location

/**
 * Represents the three possible stats for the location settings:
 * Either can be:
 *      [OK] - The user has given location permissions & the location is enabled
 *      [LOCATION_DISABLED] - The user has given location permissions but the location is turned off
 *      [REQUIRES_PERMISSION] - The user has not given location permissions
 */
enum class LocationSettingsStatus {
    OK,
    LOCATION_DISABLED,
    REQUIRES_PERMISSION
}