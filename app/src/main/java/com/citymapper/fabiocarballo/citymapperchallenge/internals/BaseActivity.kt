package com.citymapper.fabiocarballo.citymapperchallenge.internals

import android.support.v7.app.AppCompatActivity
import com.citymapper.fabiocarballo.citymapperchallenge.internals.di.ActivityModule

open class BaseActivity : AppCompatActivity() {

    val activityComponent by lazy {
        (application as BaseAppController).appComponent
                .activityComponentBuilder()
                .plus(ActivityModule(this))
                .build()
    }
}