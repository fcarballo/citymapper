package com.citymapper.fabiocarballo.citymapperchallenge.stations

import com.citymapper.fabiocarballo.citymapperchallenge.location.CoordinatePoint

data class NearbyTubeStationsBundle(
        val userLocation: CoordinatePoint,
        val stations: List<TubeStation>)