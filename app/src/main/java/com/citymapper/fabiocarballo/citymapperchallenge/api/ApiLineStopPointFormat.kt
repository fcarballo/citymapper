package com.citymapper.fabiocarballo.citymapperchallenge.api

data class ApiLineStopPointFormat(
        val id: String,
        val name: String,
        val lat: Double,
        val lon: Double)