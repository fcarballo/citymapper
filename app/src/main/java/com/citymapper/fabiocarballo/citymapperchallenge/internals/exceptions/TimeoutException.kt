package com.citymapper.fabiocarballo.citymapperchallenge.internals.exceptions

class TimeoutException : RuntimeException()