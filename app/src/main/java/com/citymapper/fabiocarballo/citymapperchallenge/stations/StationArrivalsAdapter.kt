package com.citymapper.fabiocarballo.citymapperchallenge.stations

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.citymapper.fabiocarballo.citymapperchallenge.R
import kotlinx.android.synthetic.main.elem_next_arrival.view.*
import kotlinx.android.synthetic.main.elem_station_arrivals.view.*
import org.jetbrains.anko.onClick
import java.text.DateFormat
import android.graphics.PorterDuff


class StationArrivalsAdapter(
        private val context: Context,
        private val arrivalClickListener: ArrivalClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var stationArrivalsList: List<StationArrivalsViewModel> = listOf()

    private val hoursFormatter by lazy { DateFormat.getTimeInstance(DateFormat.MEDIUM) }

    private val inflater by lazy {
        LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.elem_station_arrivals, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val view = holder.itemView
        val stationArrivals = stationArrivalsList[position]

        view.stationName.text = "${stationArrivals.name} (${stationArrivals.distanceInMeters}m)"

        if (stationArrivals.arrivals.isNotEmpty()) {
            view.nextArrivalsLabel.text = context.getString(R.string.next_arrivals)
        } else {
            view.nextArrivalsLabel.text = context.getString(R.string.next_arrivals_empty)
        }

        view.nextArrivalsContainer.removeAllViews()

        stationArrivals.arrivals
                .forEach { arrival ->
                    val arrivalView = inflater.inflate(
                            R.layout.elem_next_arrival,
                            view.nextArrivalsContainer,
                            false)

                    arrivalView.expectedArrival.text = hoursFormatter.format(arrival.expectedTime)
                    arrivalView.line.text = arrival.lineName
                    arrivalView.destination.text = arrival.destination

                    val drawable = context.getDrawable(R.drawable.circle).apply {
                        setColorFilter(getLineColor(arrival.lineId), PorterDuff.Mode.SRC)
                    }

                    arrivalView.lineColorCircle.background = drawable

                    arrivalView.onClick { arrivalClickListener.onArrivalClick(arrival) }

                    view.nextArrivalsContainer.addView(arrivalView)
                }
    }

    override fun getItemCount() = stationArrivalsList.size

    private fun getLineColor(lineId: String): Int {
        return when (lineId) {
            BAKERLOO -> ContextCompat.getColor(context, R.color.bakerloo)
            CENTRAL -> ContextCompat.getColor(context, R.color.central)
            CIRCLE -> ContextCompat.getColor(context, R.color.circle)
            NORTHERN -> ContextCompat.getColor(context, R.color.northern)
            PICADILLY -> ContextCompat.getColor(context, R.color.picaddilly)
            HAMMERSMITH -> ContextCompat.getColor(context, R.color.hammersmith)
            JUBILEE -> ContextCompat.getColor(context, R.color.jubilee)
            METROPOLITAN -> ContextCompat.getColor(context, R.color.metropolitan)
            VICTORIA -> ContextCompat.getColor(context, R.color.victoria)
            WATERLOO -> ContextCompat.getColor(context, R.color.waterloo)
            else -> ContextCompat.getColor(context, R.color.white)
        }
    }

    companion object {
        private const val BAKERLOO = "bakerloo"
        private const val CENTRAL = "central"
        private const val CIRCLE = "circle"
        private const val NORTHERN = "northern"
        private const val PICADILLY = "piccadilly"
        private const val HAMMERSMITH = "hammersmith-city"
        private const val JUBILEE = "jubilee"
        private const val METROPOLITAN = "metropolitan"
        private const val VICTORIA = "victoria"
        private const val WATERLOO = "waterloo-city"
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface ArrivalClickListener {
        fun onArrivalClick(arrival: TubeArrival)
    }
}