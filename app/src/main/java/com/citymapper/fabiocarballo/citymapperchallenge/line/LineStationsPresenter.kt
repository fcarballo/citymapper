package com.citymapper.fabiocarballo.citymapperchallenge.line

import com.citymapper.fabiocarballo.citymapperchallenge.internals.Logger
import com.citymapper.fabiocarballo.citymapperchallenge.internals.exceptions.NoConnectionException
import com.citymapper.fabiocarballo.citymapperchallenge.location.LocationSettingsStatus
import com.citymapper.fabiocarballo.citymapperchallenge.location.LocationStatusManager
import com.citymapper.fabiocarballo.citymapperchallenge.stations.TubeStation
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription
import java.util.concurrent.TimeoutException

class LineStationsPresenter(
        private val lineId: String,
        private val inbound: Boolean,
        private val originId: String,
        private val destinationId: String,
        private val view: View,
        private val locationManager: LocationStatusManager,
        private val getStationsForLine: GetLineStations,
        private val userJourneyCalculator: UserJourneyCalculator,
        private val logger: Logger) {

    private val subscriptions = CompositeSubscription()

    fun start(isSimulation: Boolean) {
        // this is needed, since we can (re)start the process in many ways possible
        // for that reason it is always needed to clear any possible existing subscriptions
        subscriptions.clear()

        getLineStations(isSimulation)
    }

    fun stop() {
        subscriptions.clear()
    }

    private fun getLineStations(isSimulation: Boolean) {
        logger.debug("requesting lines stations ...")

        subscriptions.add(
                getStationsForLine.build(lineId, inbound)
                        .map { possibleStationSequences ->
                            val stopSequencesWithOrigin = possibleStationSequences
                                    .filter { originId in it.map { it.id } }

                            val stopSequencesWithDestination = possibleStationSequences
                                    .filter { destinationId in it.map { it.id } }

                            val stopSequencesWithOriginAndDestination = possibleStationSequences
                                    .filter { destinationId in it.map { it.id } }
                                    .filter { originId in it.map { it.id } }

                            when {
                                stopSequencesWithOriginAndDestination.isNotEmpty() -> stopSequencesWithOriginAndDestination.first()
                                stopSequencesWithOrigin.isNotEmpty() -> stopSequencesWithOrigin.first()
                                else -> stopSequencesWithDestination.first()
                            }
                        }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { stationsList ->
                                    logger.debug("got ${stationsList.size} stations.")

                                    view.setStations(stationsList.map { StationViewModel(it.id, it.name) })
                                    setupLocationForUserJourneyTracking(stationsList, isSimulation)
                                },
                                handleError()))
    }

    private fun setupLocationForUserJourneyTracking(tubeStations: List<TubeStation>, isSimulation: Boolean) {
        subscriptions.add(
                locationManager.locationSettingsStatus()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { locationSettingsStatus: LocationSettingsStatus ->
                                    when (locationSettingsStatus) {
                                        LocationSettingsStatus.OK -> {
                                            view.hideDisabledLocationAlert()
                                            startUserJourneyTracking(tubeStations, isSimulation)
                                        }
                                        LocationSettingsStatus.LOCATION_DISABLED -> {
                                            view.showLocationDialog()
                                            view.showDisabledLocationAlert()
                                        }
                                        LocationSettingsStatus.REQUIRES_PERMISSION -> {
                                            locationManager.requestLocationPermission()
                                            view.showDisabledLocationAlert()
                                        }
                                    }
                                },
                                handleError()
                        )
        )
    }

    private fun startUserJourneyTracking(tubeStations: List<TubeStation>, isSimulation: Boolean) {
        logger.debug("user journey tracking will start!")
        subscriptions.add(
                userJourneyCalculator.calculateJourneyPosition(tubeStations, isSimulation)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    logger.debug("received journey update: $it")

                                    view.updateUserJourney(it)
                                },
                                handleError(),
                                { logger.debug("finished tracking user journey.") })
        )
    }

    private fun handleError(): ((Throwable) -> Unit) = {
        logger.error(it)

        when (it) {
            is NoConnectionException -> view.showConnectionIssuesPlaceholder()
            is TimeoutException -> view.showConnectionIssuesPlaceholder()
            else -> view.showUnknownErrorPlaceholder()
        }
    }

    interface View {
        fun showLocationDialog()
        fun showDisabledLocationAlert()
        fun hideDisabledLocationAlert()
        fun updateUserJourney(userJourney: UserJourney)
        fun setStations(stations: List<StationViewModel>)
        fun showConnectionIssuesPlaceholder()
        fun showUnknownErrorPlaceholder()
    }
}