package com.citymapper.fabiocarballo.citymapperchallenge.line

import com.citymapper.fabiocarballo.citymapperchallenge.location.CoordinatePoint
import com.citymapper.fabiocarballo.citymapperchallenge.stations.TubeStation
import rx.Observable
import java.util.concurrent.TimeUnit

/**
 *  Responsible for generating a list of coordinates between each of the line tube stations.
 *  This way, I can simulate that I'm entering on the first stop and doing all the path until the last station.
 */
class LocationSimulator {

    fun getLocationsFor(tubeStationList: List<TubeStation>): Observable<CoordinatePoint> {
        val coordinatesList = IntRange(0, tubeStationList.size - 2)
                .flatMap { index ->
                    val leavingStation = tubeStationList[index]
                    val approachingStation = tubeStationList[index + 1]

                    getMiddlePoints(
                            leavingStation.location,
                            approachingStation.location,
                            3)
                }

        // each two seconds a new coordinate point will be emitted

        val intervalObservable = Observable.just(0L).concatWith(Observable.interval(2000, TimeUnit.MILLISECONDS))

        return Observable.from(coordinatesList)
                .zipWith(intervalObservable, { coordinate: CoordinatePoint, _: Long -> coordinate })
    }

    private fun getMiddlePoints(start: CoordinatePoint, end: CoordinatePoint, level: Int): List<CoordinatePoint> {
        val list = mutableListOf<CoordinatePoint>()
        getPoints(list, level, start, end)

        return list
    }

    private fun getPoints(list: MutableList<CoordinatePoint>, level: Int, start: CoordinatePoint, end: CoordinatePoint) {
        if (level == 0) {
            list.add(midPoint(start, end))
        } else {
            getPoints(list, level - 1, start, midPoint(start, end))
            getPoints(list, level - 1, midPoint(start, end), end)
        }
    }

    /**
     * Retursn the coordinate point between [startPoint] and [endPoint]
     */
    private fun midPoint(startPoint: CoordinatePoint, endPoint: CoordinatePoint): CoordinatePoint {
        val dLon = Math.toRadians(endPoint.longitude - startPoint.longitude)

        //convert to radians
        val lt1 = Math.toRadians(startPoint.latitude)
        val lt2 = Math.toRadians(endPoint.latitude)
        val ln1 = Math.toRadians(startPoint.longitude)

        val bx = Math.cos(lt2) * Math.cos(dLon)
        val by = Math.cos(lt2) * Math.sin(dLon)
        val lt3 = Math.atan2(Math.sin(lt1) + Math.sin(lt2), Math.sqrt((Math.cos(lt1) + bx) * (Math.cos(lt1) + bx) + by * by))
        val ln3 = ln1 + Math.atan2(by, Math.cos(lt1) + bx)

        val midLat = Math.toDegrees(lt3)
        val midLng = Math.toDegrees(ln3)

        return CoordinatePoint(latitude = midLat, longitude = midLng)
    }
}