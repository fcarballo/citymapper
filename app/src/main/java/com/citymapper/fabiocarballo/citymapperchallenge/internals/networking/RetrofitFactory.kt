package com.citymapper.fabiocarballo.citymapperchallenge.internals.networking

import com.citymapper.fabiocarballo.citymapperchallenge.internals.RxErrorHandlingCallAdapterFactory
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.schedulers.Schedulers


class RetrofitFactory {

    fun build(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .client(client)
                .build()
    }

    companion object {
        private const val BASE_URL = "https://api.tfl.gov.uk/"
    }
}