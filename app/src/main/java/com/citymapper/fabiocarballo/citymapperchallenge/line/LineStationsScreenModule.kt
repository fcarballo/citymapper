package com.citymapper.fabiocarballo.citymapperchallenge.line

import com.citymapper.fabiocarballo.citymapperchallenge.api.TransportsForLondonUnifiedApi
import com.citymapper.fabiocarballo.citymapperchallenge.internals.Logger
import com.citymapper.fabiocarballo.citymapperchallenge.location.GetLocationUseCase
import com.citymapper.fabiocarballo.citymapperchallenge.location.LocationStatusManager
import com.citymapper.fabiocarballo.citymapperchallenge.stations.TubeArrival
import dagger.Module
import dagger.Provides

@Module
class LineStationsScreenModule(
        private val tubeArrival: TubeArrival,
        private val view: LineStationsPresenter.View) {

    @Provides
    fun providePresenter(
            locationStatusManager: LocationStatusManager,
            getStationsForLine: GetLineStations,
            userJourneyCalculator: UserJourneyCalculator,
            logger: Logger): LineStationsPresenter {
        return LineStationsPresenter(
                tubeArrival.lineId,
                tubeArrival.inbound,
                tubeArrival.arrivalStationId,
                tubeArrival.destinationId,
                view,
                locationStatusManager,
                getStationsForLine,
                userJourneyCalculator,
                logger)
    }

    @Provides
    fun provideGetStationsForLine(
            api: TransportsForLondonUnifiedApi): GetLineStations {
        return GetLineStations(api)
    }

    @Provides
    fun provideLocationSimulator(): LocationSimulator {
        return LocationSimulator()
    }

    @Provides
    fun provideUserJourneyCalculator(locationSimulator: LocationSimulator,
                                     getLocationUseCase: GetLocationUseCase): UserJourneyCalculator {
        return UserJourneyCalculator(getLocationUseCase, locationSimulator)
    }
}