package com.citymapper.fabiocarballo.citymapperchallenge.location

import android.annotation.SuppressLint
import com.google.android.gms.location.LocationRequest
import pl.charmas.android.reactivelocation.ReactiveLocationProvider
import rx.Observable
import java.util.concurrent.TimeUnit

class GetLocationUseCase(
        private val reactiveLocationProvider: ReactiveLocationProvider) {

    fun build(): Observable<CoordinatePoint> {
        return reactiveLocationProvider
                .getUpdatedLocation(locationRequest)
                .filter { it != null }
                .map { CoordinatePoint(it.latitude, it.longitude) }
                .timeout(10, TimeUnit.SECONDS, Observable.just(LONDON_CENTER))
                .map { location ->
                    if (location.distanceTo(LONDON_CENTER) < THRESHOLD_RADIUS_METERS) {
                        location
                    } else {
                        LONDON_CENTER
                    }
                }
    }

    private val locationRequest: LocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(LOCATION_UPDATES_INTERVAL_MS)
            .setFastestInterval(LOCATION_UPDATES_FASTEST_INTERVAL_MS)

    companion object {
        private const val LOCATION_UPDATES_FASTEST_INTERVAL_MS = 500L
        private const val LOCATION_UPDATES_INTERVAL_MS = 1000L

        private val LONDON_CENTER = CoordinatePoint(51.511859, -0.126350)
        private val THRESHOLD_RADIUS_METERS = 32000

    }
}