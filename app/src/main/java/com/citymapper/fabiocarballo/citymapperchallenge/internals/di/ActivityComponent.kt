package com.citymapper.fabiocarballo.citymapperchallenge.internals.di

import com.citymapper.fabiocarballo.citymapperchallenge.line.LineStationsScreenComponent
import com.citymapper.fabiocarballo.citymapperchallenge.stations.NearbyTubeStationsScreenComponent
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(ActivityModule::class))
@ActivityScope
interface ActivityComponent {

    @Subcomponent.Builder
    interface Builder {
        fun plus(module: ActivityModule): Builder
        fun build(): ActivityComponent
    }

    fun nearbyTubesScreenComponentBuilder(): NearbyTubeStationsScreenComponent.Builder

    fun lineStationsScreenComponentBuilder(): LineStationsScreenComponent.Builder
}