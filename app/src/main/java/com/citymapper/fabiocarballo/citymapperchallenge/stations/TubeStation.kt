package com.citymapper.fabiocarballo.citymapperchallenge.stations

import com.citymapper.fabiocarballo.citymapperchallenge.location.CoordinatePoint

data class TubeStation(
        val id: String,
        val name: String,
        val location: CoordinatePoint)
