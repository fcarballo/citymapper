package com.citymapper.fabiocarballo.citymapperchallenge.api

import com.google.gson.annotations.SerializedName

data class ApiTubeStationFormat(
        @SerializedName("naptanId") val id: String,
        @SerializedName("commonName") val name: String,
        @SerializedName("lat") val latitude: Double,
        @SerializedName("lon") val longitude: Double)
