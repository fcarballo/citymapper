package com.citymapper.fabiocarballo.citymapperchallenge.internals.exceptions

class NoConnectionException: RuntimeException()