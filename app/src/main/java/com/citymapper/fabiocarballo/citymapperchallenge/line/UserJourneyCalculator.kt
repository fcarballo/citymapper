package com.citymapper.fabiocarballo.citymapperchallenge.line

import android.location.Location
import com.citymapper.fabiocarballo.citymapperchallenge.internals.Logger
import com.citymapper.fabiocarballo.citymapperchallenge.location.CoordinatePoint
import com.citymapper.fabiocarballo.citymapperchallenge.location.GetLocationUseCase
import com.citymapper.fabiocarballo.citymapperchallenge.stations.TubeStation
import rx.Observable

/**
 * This class has the responsibility to provide data about the user position when moving around a line
 * of tube stations.
 *
 * To estimate the position of the user between stations, I save a temporal (limited) record of distances
 * to each tube station. With this data, I can estimate if I'm approaching or leaving the nearest station.
 *
 * This way, I'm able to return the two stations the user is between and which percentage of the path
 * was already taken.
 *
 *
 * To help me in the development of this feature, I also created the [LocationSimulator], which
 * returns a list of coordinates between each of the line tube stations. This way, I can simulate that I'm
 * entering on the first stop and doing all the path until the last station.
 */
class UserJourneyCalculator(
        private val getLocationUseCase: GetLocationUseCase,
        private val simulatorLocationManager: LocationSimulator) {

    fun calculateJourneyPosition(tubeStations: List<TubeStation>, isSimulation: Boolean = false): Observable<UserJourney> {
        // create an history of record distances for each tube station
        // using this data it is possible to understand which stations the user is approaching or leaving

        val tubeStationsDistancesHistory = tubeStations
                .associateBy({ it.id }, { StationDistanceHistory(it, LimitedSizeList(3)) })

        val locationObservable = if (isSimulation) {
            simulatorLocationManager.getLocationsFor(tubeStations)
        } else {
            getLocationUseCase.build()
        }

        return locationObservable
                .doOnNext { userLocation ->
                    // populate last seen distance to each station
                    tubeStationsDistancesHistory.values.forEach {
                        val distanceToTubeStation = userLocation.distanceTo(it.station.location)
                        it.distancesHistory.add(distanceToTubeStation)
                    }
                }
                .map { userLocation ->
                    val nearestTubeStationDistanceHistory = tubeStationsDistancesHistory.values
                            .sortedBy { it.distancesHistory.last() }
                            .first()

                    val isApproachingStation = firstDerivativeAverage(nearestTubeStationDistanceHistory.distancesHistory) < 0

                    if (isApproachingStation) {
                        val approachingStation = nearestTubeStationDistanceHistory.station
                        val indexOfPreviousStation = Math.max(tubeStations.indexOf(approachingStation) - 1, 0)
                        val leavingStation = tubeStations[indexOfPreviousStation]

                        UserJourney(
                                leavingStation.id,
                                approachingStation.id,
                                journeyPercentage(userLocation, leavingStation, approachingStation)
                        )

                    } else {
                        val leavingStation = nearestTubeStationDistanceHistory.station
                        val indexOfNextStation = Math.min(tubeStations.indexOf(leavingStation) + 1, tubeStations.size - 1)
                        val approachingStation = tubeStations[indexOfNextStation]

                        UserJourney(
                                leavingStation.id,
                                approachingStation.id,
                                journeyPercentage(userLocation, leavingStation, approachingStation)
                        )
                    }
                }
    }

    private fun firstDerivativeAverage(queue: LimitedSizeList<Float>): Double {
        var distanceDerivative = listOf<Float>()
        for (i in 1 until queue.size) {
            distanceDerivative += queue[i] - queue[i - 1]
        }
        return distanceDerivative.average()
    }

    private fun journeyPercentage(userLocation: CoordinatePoint,
                                  leavingStation: TubeStation,
                                  approachingStation: TubeStation): Float {
        val userLocationToLeavingStationDistance = userLocation.distanceTo(leavingStation.location)
        val userLocationToApproachingStationDistance = userLocation.distanceTo(approachingStation.location)

        return (userLocationToLeavingStationDistance) / (userLocationToLeavingStationDistance + userLocationToApproachingStationDistance)
    }

    class LimitedSizeList<K>(private val maxSize: Int) : ArrayList<K>() {

        override fun add(k: K): Boolean {
            val r = super.add(k)
            if (size > maxSize) {
                removeRange(0, size - maxSize - 1)
            }
            return r
        }
    }

    data class StationDistanceHistory(val station: TubeStation, val distancesHistory: LimitedSizeList<Float>)
}