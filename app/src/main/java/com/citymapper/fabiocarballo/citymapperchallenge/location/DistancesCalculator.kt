package com.citymapper.fabiocarballo.citymapperchallenge.location

/**
 * This is an helper class to help me calculate the distances. In a regular
 * scenario, I would just use the [CoordinatePoint#distanceTo] method; However,
 * the Android framework [Location] distance method doesn't run on the JVM tests.
 *
 * For that reason I have to create this wrapper to mock the results.
 */
class DistancesCalculator {

    fun distanceBetween(c1: CoordinatePoint, c2: CoordinatePoint) = c1.distanceTo(c2)
}