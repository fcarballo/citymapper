package com.citymapper.fabiocarballo.citymapperchallenge.internals

import android.view.View


fun View.toGone() {
    visibility = View.GONE
}

fun View.toVisible() {
    visibility = View.VISIBLE
}