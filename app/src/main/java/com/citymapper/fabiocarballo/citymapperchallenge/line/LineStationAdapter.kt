package com.citymapper.fabiocarballo.citymapperchallenge.line

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.citymapper.fabiocarballo.citymapperchallenge.R
import kotlinx.android.synthetic.main.elem_line_station.view.*

class LineStationAdapter(
        private val context: Context,
        private val selectedStationId: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var stationsList: List<StationViewModel> = listOf()

    private val inflater by lazy {
        LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.elem_line_station, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val view = holder.itemView
        val station = stationsList[position]

        view.topSeparator.visibility = if (position == 0) View.INVISIBLE else View.VISIBLE
        view.bottomSeparator.visibility = if (position == itemCount - 1) View.INVISIBLE else View.VISIBLE

        view.stationName.text = station.name

        val isSelectedStation = (selectedStationId == station.id)

        view.stationName.setTextSize(
                TypedValue.COMPLEX_UNIT_PX,
                context.resources.getDimension(if (isSelectedStation) R.dimen.text_size_xlarge else R.dimen.text_size_medium))
    }

    override fun getItemCount() = stationsList.size

    fun indexOf(stationId: String): Int {
        return stationsList.indexOfFirst { it.id == stationId }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}