package com.citymapper.fabiocarballo.citymapperchallenge.internals.di


interface BaseAppComponent {

    fun activityComponentBuilder(): ActivityComponent.Builder
}
