package com.citymapper.fabiocarballo.citymapperchallenge.stations

import com.citymapper.fabiocarballo.citymapperchallenge.api.TransportsForLondonUnifiedApi
import com.citymapper.fabiocarballo.citymapperchallenge.location.CoordinatePoint
import com.citymapper.fabiocarballo.citymapperchallenge.location.DistancesCalculator
import com.citymapper.fabiocarballo.citymapperchallenge.location.GetLocationUseCase
import rx.Observable
import rx.Single
import java.util.concurrent.TimeUnit

class GetNearbyTubeStations(
        private val getLocationUseCase: GetLocationUseCase,
        private val api: TransportsForLondonUnifiedApi,
        private val distancesCalculator: DistancesCalculator) {

    fun build(): Single<List<NearbyTubeStation>> {
        return getLocationUseCase.build()
                .first()
                .toSingle()
                .flatMap { userLocation ->
                    api.getTubeStations(userLocation.latitude, userLocation.longitude, RADIUS_METERS)
                            .flatMapObservable { bundle ->
                                Observable.from(bundle.stopPoints)
                            }
                            .map {
                                val stationLocation = CoordinatePoint(it.latitude, it.longitude)
                                val distanceToStation = distancesCalculator.distanceBetween(stationLocation, userLocation)

                                NearbyTubeStation(it.id, it.name, distanceToStation, stationLocation)
                            }
                            .sorted { station1, station2 -> station1.distanceInMeters.compareTo(station2.distanceInMeters) }
                            .toList()
                            .toSingle()
                }
    }

    companion object {

        private val RADIUS_METERS = 500

    }

}