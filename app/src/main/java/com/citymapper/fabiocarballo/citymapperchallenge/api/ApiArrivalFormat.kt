package com.citymapper.fabiocarballo.citymapperchallenge.api

import com.google.gson.annotations.SerializedName
import java.util.*

data class ApiArrivalFormat(
        val lineId: String = "",
        val lineName: String = "",
        val expectedArrival: Date,
        @SerializedName("destinationNaptanId") val destinationId: String,
        val destinationName: String? = "",
        val direction: String? = "") {

    fun isValid(): Boolean {
        return lineId.isNotBlank() &&
                lineName.isNotBlank() &&
                !direction.isNullOrBlank() &&
                !destinationId.isNullOrBlank() &&
                !destinationName.isNullOrBlank()
    }

}