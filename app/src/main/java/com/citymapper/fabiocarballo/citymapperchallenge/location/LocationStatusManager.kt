package com.citymapper.fabiocarballo.citymapperchallenge.location

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResult
import com.google.android.gms.location.LocationSettingsStatusCodes
import pl.charmas.android.reactivelocation.ReactiveLocationProvider
import rx.Observable
import rx.Single

/**
 * This class is responsible for requesting location permissions when the user has a device
 * whose android version already needs this permissions checks.
 */
class LocationStatusManager(
        private val reactiveLocationProvider: ReactiveLocationProvider,
        private val activity: Activity) {

    private val locationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

    fun requestLocationPermission() {
        checkPermission(LOCATION_PERMISSIONS)
    }

    /**
     * Returns the current status for the location. This will help on managing if we should request
     * permissions, show the location dialog or do nothing.
     */
    fun locationSettingsStatus(): Single<LocationSettingsStatus> {
        val locationSettingsRequest = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest).build()

        return Single.defer {
            Observable.just(hasEnabledLocationPermissions())
                    .flatMap { hasPermissions ->
                        if (hasPermissions) {
                            reactiveLocationProvider.checkLocationSettings(locationSettingsRequest)
                                    .map(LocationSettingsResult::getStatus)
                                    .map(Status::getStatusCode)
                                    .map { statusCode ->
                                        when (statusCode) {
                                            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> LocationSettingsStatus.LOCATION_DISABLED
                                            else -> LocationSettingsStatus.OK
                                        }
                                    }
                        } else {
                            Observable.just(LocationSettingsStatus.REQUIRES_PERMISSION)
                        }
                    }
                    .toSingle()
        }
    }

    private fun hasEnabledLocationPermissions(): Boolean {
        return LOCATION_PERMISSIONS.map { ContextCompat.checkSelfPermission(activity, it) }
                .map { it == PackageManager.PERMISSION_GRANTED }
                .reduce(Boolean::and)
    }

    private fun checkPermission(permissionsList: Array<String>) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            val permissionsToAsk = permissionsList.filter { !hasPermission(it) }.toTypedArray()
            val grantResults = permissionsList.map { if (hasPermission(it)) PackageManager.PERMISSION_GRANTED else PackageManager.PERMISSION_DENIED }

            if (permissionsToAsk.any()) {
                ActivityCompat.requestPermissions(activity, permissionsToAsk, LOCATION_PERMISSION_RC)
            } else {
                activity.onRequestPermissionsResult(LOCATION_PERMISSION_RC, permissionsToAsk, grantResults.toIntArray())
            }
        } else {
            ActivityCompat.requestPermissions(activity, permissionsList, LOCATION_PERMISSION_RC)
        }
    }

    private fun hasPermission(permission: String)
            = ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED


    companion object {
        private const val LOCATION_PERMISSION_RC = 2345

        private val LOCATION_PERMISSIONS = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    }
}