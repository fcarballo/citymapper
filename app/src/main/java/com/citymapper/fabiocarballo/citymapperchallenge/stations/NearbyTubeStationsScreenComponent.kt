package com.citymapper.fabiocarballo.citymapperchallenge.stations

import dagger.Subcomponent

@Subcomponent(modules = arrayOf(NearbyTubeStationsModule::class))
interface NearbyTubeStationsScreenComponent {

    @Subcomponent.Builder
    interface Builder {
        fun plus(module: NearbyTubeStationsModule): Builder
        fun build(): NearbyTubeStationsScreenComponent
    }

    fun inject(activity: NearbyTubeStationsActivity)
}