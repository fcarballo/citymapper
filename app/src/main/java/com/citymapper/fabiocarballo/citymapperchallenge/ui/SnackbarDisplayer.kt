package com.citymapper.fabiocarballo.citymapperchallenge.ui

import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.view.View
import com.citymapper.fabiocarballo.citymapperchallenge.R
import org.jetbrains.anko.backgroundColor

class SnackbarDisplayer(private val view: View) {

    private var snackbar: Snackbar? = null

    fun show(@StringRes msgResId: Int, @StringRes actionResId: Int, action: ((View) -> Unit)) {
        if (snackbar != null || snackbar?.isShown != true) {
            snackbar = Snackbar.make(view, msgResId, Snackbar.LENGTH_INDEFINITE)
                    .setAction(actionResId, action)
                    .setActionTextColor(ContextCompat.getColor(view.context, R.color.black))
                    .apply {
                        view.backgroundColor = ContextCompat.getColor(view.context, R.color.yellow)
                        show()
                    }
        }
    }

    fun hide() {
        snackbar?.let {
            if (it.isShown) {
                it.dismiss()
            }
        }
    }
}