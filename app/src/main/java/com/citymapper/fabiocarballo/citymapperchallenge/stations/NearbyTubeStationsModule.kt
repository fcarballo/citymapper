package com.citymapper.fabiocarballo.citymapperchallenge.stations

import com.citymapper.fabiocarballo.citymapperchallenge.api.TransportsForLondonUnifiedApi
import com.citymapper.fabiocarballo.citymapperchallenge.internals.Logger
import com.citymapper.fabiocarballo.citymapperchallenge.internals.Navigator
import com.citymapper.fabiocarballo.citymapperchallenge.internals.SystemTimeProvider
import com.citymapper.fabiocarballo.citymapperchallenge.location.DistancesCalculator
import com.citymapper.fabiocarballo.citymapperchallenge.location.GetLocationUseCase
import com.citymapper.fabiocarballo.citymapperchallenge.location.LocationStatusManager
import dagger.Module
import dagger.Provides

@Module
class NearbyTubeStationsModule(private val view: NearbyTubeStationsPresenter.View) {

    @Provides
    fun providePresenter(locationStatusManager: LocationStatusManager,
                         getNearbyTubeStations: GetNearbyTubeStations,
                         getArrivalsForTubeStation: GetArrivalsForTubeStation,
                         systemTimeProvider: SystemTimeProvider,
                         navigator: Navigator,
                         logger: Logger): NearbyTubeStationsPresenter {

        return NearbyTubeStationsPresenter(view,
                locationStatusManager,
                getNearbyTubeStations,
                getArrivalsForTubeStation,
                systemTimeProvider,
                navigator,
                logger)
    }

    @Provides
    fun provideGetNearbyStationsUseCase(
            getLocationUseCase: GetLocationUseCase,
            api: TransportsForLondonUnifiedApi): GetNearbyTubeStations {
        return GetNearbyTubeStations(getLocationUseCase, api, DistancesCalculator())
    }

    @Provides
    fun provideGetArrivalsForTubeStationUseCase(api: TransportsForLondonUnifiedApi,
                                                logger: Logger): GetArrivalsForTubeStation {
        return GetArrivalsForTubeStation(api, logger)
    }

}