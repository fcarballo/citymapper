package com.citymapper.fabiocarballo.citymapperchallenge.stations

import org.parceler.Parcel
import java.util.*

@Parcel
data class TubeArrival(
        val arrivalStationId: String = "",
        val lineId: String = "",
        val lineName: String = "",
        val expectedTime: Date = Date(),
        val destinationId: String = "",
        val destination: String = "",
        val inbound: Boolean = false)
