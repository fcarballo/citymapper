package com.citymapper.fabiocarballo.citymapperchallenge.stations

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.PersistableBundle
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.citymapper.fabiocarballo.citymapperchallenge.R
import com.citymapper.fabiocarballo.citymapperchallenge.internals.BaseActivity
import com.citymapper.fabiocarballo.citymapperchallenge.internals.Logger
import com.citymapper.fabiocarballo.citymapperchallenge.internals.toGone
import com.citymapper.fabiocarballo.citymapperchallenge.internals.toVisible
import com.citymapper.fabiocarballo.citymapperchallenge.location.LocationSettingsSolver
import com.citymapper.fabiocarballo.citymapperchallenge.ui.SnackbarDisplayer
import kotlinx.android.synthetic.main.activity_nearby_tubes.*
import rx.subscriptions.Subscriptions
import java.text.DateFormat
import java.util.*
import javax.inject.Inject
import com.citymapper.fabiocarballo.citymapperchallenge.R.id.recyclerView
import android.support.v7.widget.DividerItemDecoration
import com.citymapper.fabiocarballo.citymapperchallenge.location.LocationStatusManager
import kotlinx.android.synthetic.main.view_error.*
import org.parceler.Parcels
import kotlin.collections.ArrayList


class NearbyTubeStationsActivity : BaseActivity(), NearbyTubeStationsPresenter.View {

    @Inject
    lateinit var presenter: NearbyTubeStationsPresenter

    @Inject
    lateinit var locationSettingsSolver: LocationSettingsSolver

    @Inject
    lateinit var logger: Logger

    private val snackbarDisplayer by lazy { SnackbarDisplayer(container) }

    private var locationSolverSubscription = Subscriptions.empty()

    private val arrivalClickListener = object : StationArrivalsAdapter.ArrivalClickListener {
        override fun onArrivalClick(arrival: TubeArrival) {
            presenter.onArrivalClick(arrival)
        }
    }

    private val stationArrivalsAdapter by lazy { StationArrivalsAdapter(this, arrivalClickListener) }

    private val hoursFormatter by lazy { DateFormat.getTimeInstance(DateFormat.MEDIUM) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nearby_tubes)

        activityComponent
                .nearbyTubesScreenComponentBuilder()
                .plus(NearbyTubeStationsModule(this))
                .build()
                .inject(this)

        with(recyclerView) {
            layoutManager = LinearLayoutManager(this@NearbyTubeStationsActivity)
            adapter = stationArrivalsAdapter
            addItemDecoration(DividerItemDecoration(recyclerView.context, LinearLayoutManager.VERTICAL))
        }

        refreshLayout.setColorSchemeColors(
                ContextCompat.getColor(this, R.color.green),
                ContextCompat.getColor(this, R.color.yellow))

        refreshLayout.setOnRefreshListener {
            presenter.start()
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()

        locationSolverSubscription.unsubscribe()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // this was triggered by the permissions flow, let's start our location settings verification flow again
        if (locationSettingsSolver.requestIsSolved(requestCode, resultCode)) {
            presenter.start()
        } else {
            displayError(R.string.error_location)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
            // this was triggered by the permissions flow, let's start our location settings verification flow again
            presenter.start()
        } else {
            displayError(R.string.error_location_permissions)
        }
    }

    override fun showLocationDialog() {
        locationSolverSubscription = locationSettingsSolver.solveLocation()
                .subscribe(
                        { logger.debug("Executed location solving flow.") },
                        { logger.error(it) })
    }

    override fun showDisabledLocationAlert() {
        snackbarDisplayer.show(R.string.location_prompt_request, R.string.location_prompt_action) {
            presenter.start()
        }
    }

    override fun hideDisabledLocationAlert() {
        snackbarDisplayer.hide()
    }

    private fun setLoadingIconVisibility(visible: Boolean) {
        if (visible) {
            loadingIcon.toVisible()
        } else {
            refreshLayout.isRefreshing = false
            loadingIcon.toGone()
        }
    }

    override fun setStationArrivals(stationsArrivalsList: List<StationArrivalsViewModel>, timestampInMillis: Long) {
        setLoadingIconVisibility(false)
        errorView.toGone()

        if (stationsArrivalsList.isEmpty()) {
            displayError(R.string.error_no_results)
        } else {
            nearbyStationsView.toVisible()
            errorView.toGone()

            stationArrivalsAdapter.stationArrivalsList = stationsArrivalsList
            stationArrivalsAdapter.notifyDataSetChanged()
        }

        val timestampDate = Calendar.getInstance().apply { timeInMillis = timestampInMillis }.time

        lastRefresh.toVisible()
        lastRefresh.text = getString(R.string.last_refresh, hoursFormatter.format(timestampDate))
        lastRefresh.tag = timestampInMillis
    }

    override fun showConnectionIssuesPlaceholder() {
        displayError(R.string.error_network)
    }

    override fun showUnknownErrorPlaceholder() {
        displayError(R.string.error_unknown)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.let { state ->
            val stationsArrivalsList = stationArrivalsAdapter.stationArrivalsList
            val lastRefreshTimestamp = lastRefresh.tag as Long?

            if (stationsArrivalsList.isNotEmpty() && lastRefreshTimestamp != null) {

                state.putParcelable(ARRIVALS_BUNDLE, Parcels.wrap(List::class.java, stationsArrivalsList))
                state.putLong(LAST_REFRESH_TIMESTAMP, lastRefreshTimestamp)
            }
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        savedInstanceState?.let {
            if (it.containsKey(ARRIVALS_BUNDLE) && it.containsKey(LAST_REFRESH_TIMESTAMP)) {
                val stationsArrivals = Parcels.unwrap<List<StationArrivalsViewModel>>(it.getParcelable(ARRIVALS_BUNDLE))
                val lastRefreshTimestamp = it.getLong(LAST_REFRESH_TIMESTAMP)

                loadingIcon.toGone()
                setStationArrivals(stationsArrivals, lastRefreshTimestamp)
            }
        }
    }

    private fun displayError(@StringRes msgResId: Int) {
        setLoadingIconVisibility(false)

        errorView.toVisible()
        nearbyStationsView.toGone()

        errorMessage.text = getString(msgResId)
    }

    companion object {

        private const val ARRIVALS_BUNDLE = "arrivals_bundle"
        private const val LAST_REFRESH_TIMESTAMP = "timestamp"
    }
}
