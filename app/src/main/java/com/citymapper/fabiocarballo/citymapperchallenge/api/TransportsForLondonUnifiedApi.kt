package com.citymapper.fabiocarballo.citymapperchallenge.api

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import rx.Single


interface TransportsForLondonUnifiedApi {

    @GET("stoppoint/")
    fun getTubeStations(
            @Query("lat") latitude: Double,
            @Query("lon") longitude: Double,
            @Query("radius") radius: Int,
            @Query("modes") modes: String = "tube",
            @Query("returnLines") returnLines: Boolean = false,
            @Query("categories") categories: String = "none",
            @Query("stopTypes") stopTypes: String = "NaptanMetroStation"): Single<ApiTubeStationBundleFormat>

    @GET("stoppoint/{id}/arrivals")
    fun getNextArrivals(@Path("id") id: String): Single<List<ApiArrivalFormat>>

    @GET("line/{id}/route/sequence/inbound")
    fun getInboundLineStations(@Path("id") id: String): Single<ApiLineStationsBundleFormat>

    @GET("line/{id}/route/sequence/outbound")
    fun getOutboundLineStations(@Path("id") id: String): Single<ApiLineStationsBundleFormat>
}