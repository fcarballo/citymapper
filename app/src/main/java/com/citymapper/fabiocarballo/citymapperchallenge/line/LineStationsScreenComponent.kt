package com.citymapper.fabiocarballo.citymapperchallenge.line

import com.citymapper.fabiocarballo.citymapperchallenge.stations.NearbyTubeStationsActivity
import com.citymapper.fabiocarballo.citymapperchallenge.stations.NearbyTubeStationsModule
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(LineStationsScreenModule::class))
interface LineStationsScreenComponent {

    @Subcomponent.Builder
    interface Builder {
        fun plus(module: LineStationsScreenModule): Builder
        fun build(): LineStationsScreenComponent
    }

    fun inject(activity: LineStationsActivity)
}