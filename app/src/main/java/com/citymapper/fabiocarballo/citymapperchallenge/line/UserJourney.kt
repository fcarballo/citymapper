package com.citymapper.fabiocarballo.citymapperchallenge.line

import android.support.annotation.FloatRange

/**
 * Represents a point of the journey between two tube stations.
 */
data class UserJourney(
        val startingStationId: String,
        val finalStationId: String,
        @FloatRange(from = 0.0, to = 1.0) val journeyPercentage: Float)