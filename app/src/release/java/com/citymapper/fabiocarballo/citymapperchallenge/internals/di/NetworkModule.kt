package com.citymapper.fabiocarballo.citymapperchallenge.internals.di

import com.citymapper.fabiocarballo.citymapperchallenge.api.TransportsForLondonUnifiedApi
import com.citymapper.fabiocarballo.citymapperchallenge.internals.networking.HttpClientFactory
import com.citymapper.fabiocarballo.citymapperchallenge.internals.networking.RetrofitFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideApi(retrofitFactory: RetrofitFactory,
                   httpClient: OkHttpClient): TransportsForLondonUnifiedApi {
        return retrofitFactory.build(httpClient).create(TransportsForLondonUnifiedApi::class.java)
    }

    @Provides
    fun provideClient(): OkHttpClient {
        return HttpClientFactory().build()
    }

    @Provides
    fun provideRetrofitFactory(): RetrofitFactory {
        return RetrofitFactory()
    }
}