package com.citymapper.fabiocarballo.citymapperchallenge.internals.di

import dagger.Component
import javax.inject.Singleton

@Component(modules = arrayOf(
        AppModule::class,
        NetworkModule::class))
@Singleton
interface AppComponent : BaseAppComponent